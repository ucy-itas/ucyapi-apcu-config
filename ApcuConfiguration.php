<?php

namespace Iss\Api\Service\ApcuConfiguration;

use Iss\Api\Configuration\Provider\AbstractProvider;
use Iss\Api\{ServiceInterface, ServiceTrait};
use Phalcon\Config\Config;
use Phalcon\Mvc\Micro;

class ApcuConfiguration extends AbstractProvider implements ServiceInterface
{
    use ServiceTrait;

    protected function process(array $request)
    {
        [$key, $path] = $request;
        return $this->getConfiguration($key, $path);
    }

    public function get(string $key, ?string $path = null): Config
    {
        $result = $this->execute([$key, $path]);
        if (is_null($result)) {
            $result = new Config();
        }
        return $result;
    }

    private function getConfiguration(string $key, ?string $path = null): ?Config
    {
        $configuration_key = $this->getKey($key, $path);
        $configuration = apcu_fetch($configuration_key);
        if ($configuration !== false) {
            return $configuration;
        }
        return null;
    }

    public function execute(array $request)
    {
        $configuration = parent::execute($request);
        if (!is_null($configuration)) {
            [$key, $path] = $request;
            $configuration_key = $this->getKey($key, $path);
            apcu_store($configuration_key, $configuration);
        }
        return $configuration;
    }

    public function register(Micro $application): bool
    {
        if (!$application->getDI()->has('config')) {
            return false;
        }
        $original_config = $application->getDI()->get('config');
        $this->appendSuccessor($original_config);
        $application->getDI()->remove('config');
        $application->getDI()->setShared('config', $this);
        return true;
    }

    public function unregister(Micro $application): ?ServiceInterface
    {
        $parent = null;
        $config = $application->getDI()->get('config');
        while (true) {
            if (is_null($config)) {
                break;
            }
            if ($config instanceof ApcuConfiguration) {
                if ($parent === null) {
                    $application->getDI()->remove('config');
                    $application->getDI()->setShared('config', $config->getSuccessor());
                } else {
                    $parent->setSuccessor($config->getSuccessor());
                    $application->getDI()->remove('config');
                    $application->getDI()->setShared('config', $parent);
                }
                break;
            }
            $parent = $config;
            $config = $config->getSuccessor();
        }
        return $this;
    }

    public static function getName(): string
    {
        return 'apcu-config';
    }

    protected function getKey(string $key, ?string $path = null): string
    {
        return $key . '@' . ($path ? $path : 'config');
    }
}